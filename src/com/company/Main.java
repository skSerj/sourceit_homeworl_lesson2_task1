package com.company;

public class Main {

    public static void main(String[] args) {
        // Задание №1: Дано три различных числа, подсчитать сумму квадратов двух наибольших чисел.
        // Результат вывести в консоль.
        int a = 2;
        int b = 3;
        int c = 1;


        if (a < b && a < c) {
            System.out.println("сумма квадратов наибольших чисел = " + (Math.pow(b, 2) + Math.pow(c, 2)));
        } else if (b < a && b < c) {
            System.out.println("сумма квадратов наибольших чисел = " + (Math.pow(a, 2) + Math.pow(c, 2)));
        } else {
            System.out.println("сумма квадратов наибольших чисел = " + (Math.pow(a, 2) + Math.pow(b, 2)));
        }
    }
}